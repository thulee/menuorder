var mongoose = require('mongoose');
var config = require('../consts/const');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    fullname: {
        type: String,
        required: false
    },
    permission: {
        type: Number,
        default: 0
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }, 
    avatar: {
        type: String,
        default: config.STATUS_DATA
    },
    foodsFavourite: {
        type: [Schema.Types.ObjectId],
        ref: 'food'
    }
});

var user = mongoose.model('user', userSchema);

module.exports = user;