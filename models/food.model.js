var mongoose = require('mongoose');
var config = require('../consts/const');
var Schema = mongoose.Schema;

var foodSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        default: config.STATUS_DATA
    },
    ingredients: {
        type: String,
        default: config.STATUS_DATA
    },
    resource: {
        type: String,
        default: config.STATUS_DATA
    },
    price: {
        type: Number,
        required: true
    }
});

var food = mongoose.model('food', foodSchema);

module.exports = food;