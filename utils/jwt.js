var jwt = require('jsonwebtoken');
var fs = require('fs');
var publicKey = fs.readFileSync(__dirname + '/key/key.pub');
var privateKey = fs.readFileSync(__dirname + '/key/key.pem');

exports.sign = function(obj, callback){
    jwt.sign(obj, privateKey, { algorithm: 'RS256' }, function(err, token){
        callback(err, token);
    }); 
}

exports.verify = function(token, callback){
    jwt.verify(token, publicKey, function(err, decoded){
        callback(err, decoded);
    });
}