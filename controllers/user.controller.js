var User = require('../models/user.model');
var crypto = require('crypto');
var path = require('path');
var jwt = require('../utils/jwt');
var consts = require('../consts/const');
var error = require('../consts/error');
var secretKey = process.env.SECRET_KEY;

module.exports = {
    getDetailUSer: getDetailUSer,
    createUser: createUser,
    updateUser: updateUser,
    deleteUser: deleteUser,
    uploadAvatar: uploadAvatar,
    getAllUser: getAllUser
};

function getDetailUSer(email) {
    return User.findOne({ email: email })
        .then((user) => {
            return Promise.resolve(user);
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function createUser(newUser) {
    return User.find({ $or: [{ "username": newUser.username }, { "email": newUser.email }] })
        .then((userFinded) => {
            if (userFinded.length > 0) {
                return new Promise((resolve, reject) => {
                    userFinded.forEach((element) => {
                        if (element.username == newUser.username) {
                            return reject( {
                                statusCode: 400,
                                message: error.USERNAME_EXIST
                            })
                        } else if(element.email == newUser.email) {
                            return reject({
                                statusCode: 400,
                                message: error.EMAIL_EXIST
                            })
                        } 
                    })
                })
               
            } else {
                var hashPassword = crypto.createHmac('sha256', secretKey)
                    .update(newUser.password)
                    .digest('hex');
                newUser.password = hashPassword;
                var user = new User(newUser);
                return user.save()
                    .then((data) => {
                        return Promise.resolve(data);
                    })
                    .catch((err) => {
                        return Promise.reject(err);
                    });
            }
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function updateUser(username, body) {
    return User.findOne({ username: username })
        .then((_res) => {
            if(_res){
                return User.updateOne({ username: username }, body)
                    .then(() => {
                        return Promise.resolve(Object.assign(_res, body));
                    })
                    .catch((err) => {
                        return Promise.reject(err);
                    })
            } else {
                return Promise.reject({
                    statusCode: 404,
                    message: error.USER_NOT_FOUND
                })
            }
           
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}

function deleteUser(username) {
    return User.findOne({ username: username })
        .then((_res) => {
            if(_res){
                return User.deleteOne({ username: username })
                    .then(() => {
                        return Promise.resolve(_res);
                    })
                    .catch((err) => {
                        return Promise.reject(err);
                    })
            } else {
                return Promise.reject({
                    statusCode: 404,
                    message: error.USER_NOT_FOUND
                })
            }
           
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}

function uploadAvatar(username, file) {
    return User.findOne({ username: username })
        .then((user) => {
            if (user) {
                return new Promise((resolve, reject) => {
                    file.mv(path.join(__dirname, '../public/avatar/user_' + user._id + '.png'), (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return User.update({ username: username }, { $set: { avatar: 'user_' + user._id + '.png' } })
                            .then(() => {
                                user.avatar = 'user_' + user._id + '.png';
                                return resolve(user);
                            })
                            .catch((err) => {
                                return reject(err);
                            })
                    });
                });
            } else {
                return Promise.reject({
                    message: error.UPLOAD_AVATAR_FAIL,
                    statusCode: 404
                })
            }

        })
        .catch((err) => {
            return Promise.reject(err);
        })
}

function getAllUser(_nameSearch, page, type) {
    return User.find({username: {$regex: _nameSearch, $options: 'i'}}).limit(5).skip((page-1)*5).sort({username: type})
        .then(user => {
            return Promise.resolve(user);
        })
        .catch(err => {
            return Promise.reject(err);
        })
}