var User = require('../models/user.model');
var crypto = require('crypto');
var jwt = require('../utils/jwt');
var Config = require('../consts/error');

var secretKey = process.env.SECRET_KEY;

module.exports = {
    login: login,
    checkAuth: checkAuth,
    checkDB: checkDB
}

function login(username,email, password){
    var hashPassword = crypto.createHmac('sha256', secretKey).update(password).digest('hex');
    return User.find({ username: username, password: hashPassword  })
        .then((user) => {
            if(user.length > 0){
                console.log('user', user)
                return new Promise((resolve, reject) => {
                    jwt.sign({ email: user.email }, function(err, token){
                        if(err){
                            return reject({
                                statusCode: 400,
                                message: err.message
                            });
                        }else{
                            return resolve({
                                token: token,
                                user: user
                            });
                        }
                    });
                });
            }else{
                return Promise.reject({
                    statusCode: 400,
                    message: Config.USER_INCORRECT_UP
                });
            }
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function checkAuth(user){
    return User.findOne(user)
        .then((foundUser) => {
            if(foundUser){
                return Promise.resolve(foundUser);
            }else{
                return Promise.reject({
                    message: Config.USER_NOT_FOUND
                });
            }
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function checkDB(){
    return User.find().then(
        res => {
            return res;
        }
    )
}
