//text
const STATUS_DATA = 'Đang cập nhật';
const SUCCESS = 'Thành công!';
const NOT_AUTHORIZED = 'Not authorized!';
const RATE_LIMIT_TIME = 'Fifty-five';
const RE_PASSWORD_SUCCESS = 'Bạn đã đổi mật khẩu thành công!';
// like
const LIKE_SUCCESS = 'Bạn đã LIKE thành công!';
const UNLIKE_SUCCESS = 'Bạn đã UNLIKE thành công!';
//number
const THIRTY_MINUTES = 1800000;

module.exports = {
     STATUS_DATA,
     SUCCESS,
     NOT_AUTHORIZED,
     THIRTY_MINUTES,
     RATE_LIMIT_TIME,
     LIKE_SUCCESS,
     UNLIKE_SUCCESS,
     RE_PASSWORD_SUCCESS
};