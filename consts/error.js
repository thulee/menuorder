// Singer
const SINGER_NOT_FOUND = 'This singer not found!';
const CAN_NOT_UPDATE_SINGER = 'You can not update this artist';
const CAN_NOT_DELETE_ARTIST = 'You can not delete this Artist';
const REQUIREMENT_NAME = 'Name is required!';
// Album
const ALBUM_EXIST = 'Album really existed!';
const ALBUM_NOT_FOUND = 'This album not found';
const CAN_NOT_UPDATE_ALBUM = 'You can not update this album!';
const CAN_NOT_DELETE_ALBUM = 'You can not delete this album!';
const REQUIREMENT_TITLE = 'Title is required!';
//user
const USER_INCORRECT_UP = 'Username or Password incorrect!';
const USER_INCORRECT_UM = 'Username or Email is required!';
const USER_NOT_FOUND = 'Not found user!';
const REQUIREMENT_PASSWORD = 'Password is required!';
const CAN_NOT_UPDATE_USER = 'You can not delete this user!';
const CAN_NOT_DELETE_USER = 'You can not delete this album!';
const FORBIDDEN = 'Forbidden!';

const MSG_FORGET_PASSWORD =  'Yêu cầu đổi lại mật khẩu đã được gửi qua mail của ban!';
const REQUIREMENT_NEW_PASSWORD = 'Bạn chưa nhập mật khẩu mới!';
const USERNAME_EXIST = 'Username exist!';
const EMAIL_EXIST = 'Email exist!';

const REQUIREMENT_USERNAME = 'User name invalid!';
const REQUIREMENT_EMAIL = 'Email invalid!';
// files
const NO_FILE_UPDATE = 'No files were uploaded';
const UPLOAD_AVATAR_FAIL = 'up load avatar fail!';
const TOKEN_INVALID = 'Invalid token!';
const LINK_INVALID = 'Link invalid!';

module.exports = {
    CAN_NOT_DELETE_ARTIST,
    SINGER_NOT_FOUND,
    CAN_NOT_UPDATE_SINGER,
    REQUIREMENT_NAME,
    ALBUM_EXIST,
    ALBUM_NOT_FOUND,
    CAN_NOT_UPDATE_ALBUM,
    CAN_NOT_DELETE_ALBUM,
    REQUIREMENT_TITLE,
    USER_INCORRECT_UP,
    USER_INCORRECT_UM,
    USER_NOT_FOUND,
    REQUIREMENT_PASSWORD,
    FORBIDDEN,
    CAN_NOT_UPDATE_USER,
    NO_FILE_UPDATE,
    REQUIREMENT_EMAIL,
    MSG_FORGET_PASSWORD,
    REQUIREMENT_NEW_PASSWORD,
    USERNAME_EXIST,
    EMAIL_EXIST,
    UPLOAD_AVATAR_FAIL,
    TOKEN_INVALID,
    LINK_INVALID,


    REQUIREMENT_USERNAME
    
};