var authController = require('../controllers/auth.controller');
var jwt = require('../utils/jwt');
var config = require('../consts/const');

exports.auth = function(){
    return function(req, res, next){
        var token = req.headers['x-access-token'];
        //console.log(token);
        if(token){
            jwt.verify(token, function(err, decoded){
                if(err){
                    res.status(401).json({
                        message: 'Invalid token!'
                    });
                }else{
                    var email = decoded.email;
                    console.log('role', decoded.email);
                    authController.checkAuth({ email: email })
                        .then((user) => {
                            req.user = user;
                            next();
                        })
                        .catch((err) => {
                            res.status(401).json({
                                message: 'Invalid token! Not found user'
                            });
                        });
                }
            });
        }else{
            res.status(401).json({
                message: config.NOT_AUTHORIZED
            });
        }
    }
}