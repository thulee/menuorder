var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');
var dotenv = require('dotenv').config();
var crypto = require('crypto');
var fs = require('fs');

// var gmailAuth = require('./libs/gmail.auth');
var User = require('./models/user.model');
var errorHandler = require('./middle-ware/error-handler');
var userRouters = require('./routes/user.router');
var loginRouters = require('./routes/auth.router');

var port = process.env.PORT;
// var mLabUrl = process.env.MLAB_DB
var localUrl = process.env.LOCAL_DB
var passwordAD = process.env.PASSWORD_AD;
var usernameAD= process.env.USERNAME_AD;
var emailAD = process.env.EMAIL_AD;
var secretKey = process.env.SECRET_KEY;

var app = express();

var allowCrossDomain = function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');
    next();
};

app.use(allowCrossDomain);
app.use(express.static('public'));
app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api/user', userRouters);
app.use('/api/login', loginRouters);


mongoose.connect(localUrl, { useNewUrlParser: true }, err => {
    if (err) {
        console.log('Not connect to the database!');
    } else {
        console.log('Successfully connected to MongoDB')
    }
});

app.use(errorHandler.errorHandler());

admin();

// Load client secrets from a local file.

// fs.readFile(__dirname + '/libs/configs/credentials.json', (err, content) => {
//     if (err) return console.log('Error loading client secret file:', err);
//     // Authorize a client with credentials, then call the Gmail API.
//     gmailAuth.authorize(JSON.parse(content));
//   });

app.listen(port, () => {
    console.log(`Server is listening on port ${port}...`);
})

function admin(){
    var hashPassword = crypto.createHmac('sha256', secretKey).update(passwordAD).digest('hex');
    var infor = {
        username: usernameAD,
        email: emailAD,
        password: hashPassword,
        permission: 1,
        isVip: true
    }
    var user = new User(infor);
    User.find({ email: infor.email })
        .then((userFinded) => {
            if(userFinded.length === 0){
                user.save();
            }
        })
}