var userController = require('../controllers/user.controller');
var User = require('../models/user.model');
var router = require('express').Router();
var auth = require('../middle-ware/auth');
var config = require('../consts/const');
const nodeMailer = require('nodemailer');
var Joi = require('@hapi/joi');
var error = require('../consts/error');
var success = require('../consts/const')


router.post('/detail', auth.auth(), getDetailUSer);
router.post('/', creatUser);
router.put('/:username', auth.auth(), updateUser);
router.delete('/:username', auth.auth(), deleteUser);
router.post('/avatar/:username', auth.auth(), uploadAvatar);
router.post('/getAllUser', getAllUser);

module.exports = router;

function getDetailUSer(req, res, next) {
    var email = req.user.email;
    userController.getDetailUSer(email)
        .then((user) => {
            return res.send(user);
        })
        .catch((err) => {
            return next(err);
        });
}

function creatUser(req, res, next) {
    var newUser = req.body;
    if(!newUser.username){
        next({
            statusCode: 400,
            message: error.REQUIREMENT_USERNAME
        })
    } else if(!newUser.email) {
        next({
            statusCode: 400,
            message: error.REQUIREMENT_EMAIL
        })
    }  else if(!newUser.password) {
        next({
            statusCode: 400,
            message: error.REQUIREMENT_PASSWORD
        })
    } else {
        userController.createUser(newUser)
        .then( result => {
            return res.json({
                statusCode: 200,
                message: config.SUCCESS,
                data: result
            })
        })
        .catch( err => {
            return res.json({message: err})
        })
    }
    
}

function updateUser(req, res, next) {
    var data = req.body;
    console.log('update', req.user);
    var userName = req.params.username;
    if (req.user.username == userName) {
        userController.updateUser(userName, data)
            .then((result) => {
                    return res.json({
                        statusCode: 200,
                        message: config.SUCCESS,
                        userUpdated: result
                    })
                }
            )
            .catch(err => { return res.json({ message: err }) })
    } else {
        return next({
            statusCode: 403,
            message: error.FORBIDDEN
        });
    }
}

function deleteUser(req, res, next) {
    var _username = req.params.username;
    if(req.user.permission == 1 || req.user.username == _username){
        userController.deleteUser(_username)
            .then((result) => {
                    return res.json({
                        message: config.SUCCESS,
                        statusCode: 200,
                        userDeleted: result
                    })
                }
            )
            .catch(err => { return res.json({ message: err }) })
    } else {
        return next({
            statusCode: 403,
            message: error.CAN_NOT_UPDATE_USER
        });
    }
}

function uploadAvatar(req, res, next) {
    var _username = req.params.username;
    if (!req.files) {
        return res.status(400).send(error.NO_FILE_UPDATE);
    }
    let file = req.files.file;
    userController.uploadAvatar(_username, file)
        .then((user) => {
            return res.send({
                user: user
            })
        })
        .catch((err) => {
            return next(err);
        })
}

function getAllUser(req, res, next){
    var data = req.body;
    userController.getAllUser(data.username, data.page, data.type)
        .then((data) => {
            return res.send(data);
        })
        .catch((err) => {
            return res.send(err);
        })
}

