var router = require('express').Router();
var authController = require('../controllers/auth.controller');
var Config = require('../consts/error');

router.post('/', login);

module.exports = router;

function login(req, res, next) {
    console.log(req.body);
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;
    if (!password) {
        return next({
            statusCode: 400,
            message: Config.REQUIREMENT_PASSWORD
        });
    } else if (!username && !email) {
        console.log('adj')
        return next({
            statusCode: 400,
            message: Config.USER_INCORRECT_UM
        });
    } else {
        console.log('add')
        authController.login(username, email, password)
            .then((result) => {
                console.log('res',result )
                return res.send(result);
            })
            .catch((err) => {
                return next(err);
            })
    }
}